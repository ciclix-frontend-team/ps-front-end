# ps-front-end

Repositório do processo seletivo de front-end

---

## Objetivo

Construir uma interface que consiga se comunicar com o nosso servidor de testes e mostre os dados recebidos.

Você deve usar o wireframe a seguir como um guia no desenvolvimento deste desafio:

- Link para o AdobeXD: https://xd.adobe.com/view/b815e280-d4c8-438e-8a70-10868f83709e-ed46/?fullscreen



- Uri do servidor: wss://ps-1-fmfnvf266a-rj.a.run.app/ws

## Modelos de dados

#### Modelo de entrada de dados

```json
{
	"command": "nome do comando"
}
```

#### Modelo de saída de dados

```json
{
	"status": "identificador da mensagem",
	"data": "corpo da mensagem"
}
```

---

## Comandos e mensagens

### Comandos

#### Comando de query de beacons

```json
{
	"command": "queryBeacons"
}
```

### Mensagens

#### Erro

```json
{
	"status": "error",
	"data": "error message"
}
```

#### Resposta da query de beacons

```json
{
	"status": "beacon-query-response",
	"data": [
		{
			"mac": "AC:23:3F:A2:2C:01",
			"type": "Staff",
			"location": ""
		},
		{
			"mac": "AC:23:3F:A2:2C:05",
			"type": "Staff",
			"location": ""
		}
	]
}
```

#### Atualização da localização

```json
{
	"status": "location-update",
	"data": {
		"mac": "AC:23:3F:A2:2C:01",
		"location": "corredor1"
	}
}
```
