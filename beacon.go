package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/gorilla/websocket"
)

var beaconList []BeaconModel

var macList []string = []string{

	"AC:23:3F:A2:2C:01",
	"AC:23:3F:A2:2C:05",
	"AC:23:3F:A2:2B:EF",
	"AC:23:3F:A2:2B:DF",
	"AC:23:3F:A2:2B:E3",
	"AC:23:3F:A2:2C:08",
}

var locationList []string = []string{
	"corredor1",
	"sala de cirurgia1",
	"corredor2",
}

func initBeacons() {

	beaconsFile, err := ioutil.ReadFile("beacons.json")

	if err != nil {

		log.Println(err.Error())

		os.Exit(1)

		return
	}

	var beacons []BeaconModel

	err = json.Unmarshal(beaconsFile, &beacons)

	if err != nil {

		log.Println(err.Error())

		os.Exit(1)

		return
	}

	beaconList = beacons

	go generateLocationManager()

}

func handleBeaconQuery(socket *websocket.Conn) {

	sendMessageToClient(socket, SocketOutgoingMessage{Status: "beacon-query-response", Data: beaconList})

}

func generateLocationManager() {

	for {
		<-time.After(time.Second)
		go generateLocation()
	}

}

func generateLocation() {

	mac := macList[rand.Intn(len(macList))]

	location := locationList[rand.Intn(len(locationList))]

	var locationUpdate LocationUpdate = LocationUpdate{
		Mac:      mac,
		Location: location,
	}

	sendMessageToClients(SocketOutgoingMessage{Status: "location-update", Data: locationUpdate})

}
