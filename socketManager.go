package main

import (
	"encoding/json"
	"log"
	"strconv"
	"sync"

	"github.com/gorilla/websocket"
)

var clients map[*websocket.Conn]bool = make(map[*websocket.Conn]bool)
var mutex sync.Mutex

func registerConnection(socket *websocket.Conn) {
	clients[socket] = true

	log.Println("Connections: " + strconv.Itoa(len(clients)))
}

func unregisterConnection(socket *websocket.Conn) {
	delete(clients, socket)
	socket.Close()
	log.Println("Connections: " + strconv.Itoa(len(clients)))
}

func sendMessageToClient(socket *websocket.Conn, msg SocketOutgoingMessage) {

	mutex.Lock()

	defer mutex.Unlock()

	byteMsg, err := json.Marshal(msg)

	if err != nil {

		log.Println(err)

		return

	}

	err = socket.WriteMessage(websocket.TextMessage, byteMsg)

	if err != nil {

		log.Println(err)

		return
	}
}

func sendMessageToClients(msg SocketOutgoingMessage) {
	for k := range clients {
		sendMessageToClient(k, msg)
	}
}
