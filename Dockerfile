FROM golang:1.13.10-alpine3.10

RUN apk add gcc libc-dev

WORKDIR /go/src/ps-front-end
COPY ./go.mod ./go.sum ./
RUN go mod download

WORKDIR /var/ps-front-end
COPY . .
RUN go build -tags musl

EXPOSE 8080

CMD ["./ps-front-end"]