package main

type SocketIncomingMessage struct {
	Command string `json:"command"`
}

type SocketOutgoingMessage struct {
	Status string      `json:"status"`
	Data   interface{} `json:"data"`
}

type BeaconModel struct {
	Mac      string `json:"mac"`
	Type     string `json:"type"`
	Location string `json:"location"`
}

type LocationUpdate struct {
	Mac      string `json:"mac"`
	Location string `json:"location"`
}
