package main

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"

	"github.com/gorilla/websocket"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

var m = http.NewServeMux()

var server http.Server

func main() {

	var listPort string = ":8080"

	// if len(os.Getenv("PORT")) != 0 {
	// 	listPort = os.Getenv("PORT")
	// }

	server = http.Server{
		Handler: m,
		Addr:    listPort,
	}

	initBeacons()

	var ctx = context.Background()

	m.HandleFunc("/", pingHandler)

	m.HandleFunc("/ws", wsHandler)

	go handleShutdown(ctx)

	log.Println("starting server")

	var err = server.ListenAndServe()

	if err != nil {
		log.Println(err.Error())
	}

}

func handleShutdown(ctx context.Context) {
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt)

	<-stop

	server.Shutdown(ctx)

}

func pingHandler(writer http.ResponseWriter, request *http.Request) {
	fmt.Fprintf(writer, "pong\n")
}

func wsHandler(writer http.ResponseWriter, request *http.Request) {

	socket, err := upgrader.Upgrade(writer, request, nil)

	if err != nil {
		log.Println(err)
		return
	}

	defer func() {
		unregisterConnection(socket)
	}()

	registerConnection(socket)

	var check bool = true

	for check {

		check = checkForSocketMessages(socket)

	}

}

func checkForSocketMessages(socket *websocket.Conn) bool {

	_, msg, err := socket.ReadMessage()

	if err != nil {

		return false

	}

	err = handleIncomingSocketMessage(msg, socket)

	if err != nil {

		sendMessageToClient(socket, SocketOutgoingMessage{Status: "Error", Data: "error handling message, check error module for details"})

		return true
	}

	return true

}

func handleIncomingSocketMessage(message []byte, socket *websocket.Conn) (err error) {

	var command SocketIncomingMessage

	err = json.Unmarshal(message, &command)

	if err != nil {

		sendMessageToClient(socket, SocketOutgoingMessage{Status: "error", Data: "could not parse message"})

		return err

	}

	log.Println(command.Command)

	if command.Command == "queryBeacons" {

		handleBeaconQuery(socket)

		return nil

	}

	sendMessageToClient(socket, SocketOutgoingMessage{Status: "error", Data: "unknown command"})

	return nil

}
